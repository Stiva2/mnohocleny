import awh.IntList;

/*

java Mnohocleny plus 2 3 -- 1 4 5
(2x + 3) + (1x**2 + 4x + 5)

 */
public class Mnohocleny {
    public static void main(String[] args) {
        if (args.length < 4) {
            vypisNapovedu();
            return;
        }
        String operace = args[0];
        awh.IntList koef[] = new awh.IntList[] {
                IntList.create(),
                IntList.create()
        };
        int koefIndex = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                koefIndex = 1;
                continue;
            }
            koef[koefIndex].add(Integer.parseInt(args[i]));
        }

        Mnohoclen a = new Mnohoclen(koef[0]);
        Mnohoclen b = new Mnohoclen(koef[1]);
        Mnohoclen vysledek;
        if (operace.equals("plus")) {
            vysledek = a.plus(b);
        } else if (operace.equals("minus")) {
            vysledek = a.minus(b);
        } else {
            vypisNapovedu();
            return;
        }
        if (operace.equals("plus")){
            System.out.printf("(%s) + (%s) = (%s)\n",
                a, b, vysledek);
        }
        if (operace.equals("minus")){
          System.out.printf("(%s) - (%s) = (%s)\n",
              a, b, vysledek);
        }
    }

    private static void vypisNapovedu() {
        System.out.println("Napiste plus/minus a b c ... -- a b c ...");
    }

}
