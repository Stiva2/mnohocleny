public class MnohoclenTest {
    public static void
    testujToString(String ocekavany, awh.IntList koef) {
        Mnohoclen mc = new Mnohoclen(koef);
        String vystup = mc.toString();
        if (!ocekavany.equals(vystup)) {
            System.out.printf(
                "CHYBA[toString]: ocekavam \"%s\", mam \"%s\".\n",
                ocekavany, vystup);
        }
    }

    public static void
    testujGetDegree(int ocekavany, awh.IntList koef) {
        Mnohoclen mc = new Mnohoclen(koef);
        int vystup = mc.getDegree();
        if (vystup != ocekavany) {
            System.out.printf(
                "CHYBA[getDegree]: ocekavam %d, mam %d.\n",
                ocekavany, vystup);
        }
    }

    public static void
    testujGetCoef(int... koef) {
        Mnohoclen mc = new Mnohoclen(awh.IntList.create(koef));
        int stupen = mc.getDegree();
        if (stupen != koef.length - 1) {
            System.out.printf(
                "CHYBA[getCoef]: stupen=%d, koef.length=%d\n",
                stupen, koef.length);
            return;
        }
        for (int i = 0; i < koef.length; i++) {
            int skutecny = mc.getCoef(i);
            int ocekavany = koef[koef.length-i-1];
            if (ocekavany != skutecny) {
                System.out.printf(
                    "CHYBA[getCoef]: i=%d skutecny=%d, ocekavany=%d\n",
                    i, skutecny, ocekavany);
                return;
            }
        }
    }

    public static void main(String[] args) {
        testujGetDegree(2, awh.IntList.create(1, 4, 5));
        testujGetDegree(1, awh.IntList.create(7, 8));

        testujGetCoef(2, 3);
        testujGetCoef(1, 4, 5);

        testujToString("2x + 3",
                awh.IntList.create(2, 3));
        testujToString("1x^2 + 4x + 5",
                awh.IntList.create(1, 4, 5));
    }

}
